# Docker setup for local ASO FE development

Installs Nginx and AEM Publish (todo AEM Author) into docker containers 

## Local machine setup
1. Add entry to the /etc/hosts file ```127.0.0.1 local.academy.com```
1. Install `nvm` Node version package manager and run `nvm install 10` to install Node version 10


## Docker Installation

### Mac community edition
https://hub.docker.com/editions/community/docker-ce-desktop-mac

### Pull this repo

### Copy the following files into the `./aem/downloads` folder
- cq6-publish-p4505.jar
- license.properties

### Copy the following files into the `./nginx/downloads` folder
- localhost.crt
- localhost.key

### Start ALL ASO Docker containers
Navigate to the repo root and run the following command to start the containers.  Please wait several minutes for
AEM containers to install into Docker volumes when starting containers for the first time.  _See Setup System User_

```
docker-compose up -d
```

### Setup *Publish*  System User (First time containers start)
This step is only required when starting the containers for the first time.

#### AEM Setup
1. confirm that AEM Publish (Quickstart) started by checking the logs using command `docker-compose logs -f publish`.  CTRL-C to exit logs tracing.  Do NOT proceed to the next steps until AEM quickstart has started.
1. create a `install` subdirectory in the generated `data-aem-publish` folder relative to repo root and copy the following files into `data-aem-publish/install`, (e.g `mkdir data-aem-publish/install`) 
    - academysports.ui.apps-1.0-SNAPSHOT.zip
    - AEM-COntent.zip
1. goto http://localhost:4505/crx/explorer/login.jsp and login as `admin/admin`.  If link is not working, check the pubish container logs to confirm that quickstart has started.
1. click on the "User Administration" link top User Administration modal
1. click the "Create System User" button in toolbar
1. enter `academyServiceUser` into the UserID field
1. click the green icon on bottom right corner
1. restart AEM publish container `docker-compose restart publish`. Wait a few minutes for AEM to restart

#### Fix blank page issue.
If you see a blank page when visiting https://local.academy.com:
1. visit http://localhost:4505/crx/de/index.jsp
1. login as admin/admin by clicking the login button in toolbar above frames.
1. browse to `/apps/academysports/components/page/basePage/headerlibs.html/jcr:content`
1. replace the line of code  `opacity: 0` with `opacity: 1` and save (use `cmd+s` on mac)


### Stop ALL ASO Docker containers

```
docker-compose down
```

### Restart AEM Publish container

```
docker-compose restart publish
```

### Check AEM Publish logs
```
docker-compose logs -f publish
```

### Attach to running AEM container
```
docker exec -it academy_publish_1 sh
```