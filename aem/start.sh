#!/bin/sh

DEFAULT_START_OPTS='-use-control-port -nofork -nobrowser -verbose '

cd / && exec java $CQ_JVM_OPTS -jar /cq6-publish-p4505.jar $DEFAULT_START_OPTS "$@"